﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRecorder : MonoBehaviour
{
    [Header("Script References")]
    public CloneSpawner spawner;
    public CloneMovement firstClone;

    [Header("Timers")]
    [Tooltip("How often to spawn a new clone")]
    public float spawnInterval = 1f;
    [Tooltip("How often to record the player's position")]
    public float recordInterval = 0.2f;

    //Make it a queue so we can pop the first off when lerping through
    //A Queue of positions the player has passed through, to send to the clones so they can "follow" the players path.
    private Queue<Vector3> recordedPositons = new Queue<Vector3>();
    //Also send the rotations
    private Queue<Quaternion> recordedRotations = new Queue<Quaternion>();

    //Have the clone wars begun?
    private bool startedSpawning = false;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;


        recordTimer = recordInterval;
        sendTimer = spawnInterval;
    }

    //Timers (they get set in start, don't worry)
    private float recordTimer = 999999;
    private float sendTimer = 999999;

    // Update is called once per frame
    void Update()
    {
        //If we havent started spawning clones yet
        if (!startedSpawning)
        {
            //Wait for the player to start moving
            if (this.GetComponent<Rigidbody>().velocity.magnitude > 0.01)
            {
                //Debug.Log("Players speed when spawning started: " + this.GetComponent<Rigidbody>().velocity.magnitude);
                //Then start spawning clones.
                startedSpawning = true;
                spawner.StartSpawning(spawnInterval);
                //This way they have a momeny upon starting/restarting to create a path in their mind.
            }


            //Don't go on down to the timers yet, we don't need to waste time trying to send data when there's nothing to send it to!
            //Thats why i put it in the else
        }
        else
        {
            if (sendTimer > 0)
            {
                if (recordTimer > 0)
                {
                    recordTimer -= Time.deltaTime;
                }
                //else reset it and record the current position
                else
                {
                    //Add this position
                    recordedPositons.Enqueue(this.transform.position);
                    recordedRotations.Enqueue(this.transform.rotation);
                    recordTimer = recordInterval;
                }
                sendTimer -= Time.deltaTime;
            }
            //Send the list and clear it.
            else
            {
                //Send the current list
                //if the first clone is null, delay for the spawner to make the clone
                if (firstClone == null)
                {
                    sendTimer = 0.02f;
                }
                else
                {
                    firstClone.SendList(recordedPositons, recordedRotations); //There's a nullref here on the first call because it happens before the spawner sets it.
                    //Debug.Log("Sent list at time " + Time.time);

                    //Make a new list
                    recordedPositons = new Queue<Vector3>();
                    recordedRotations = new Queue<Quaternion>();
                    //"Reset the Clock"
                    sendTimer = spawnInterval;
                }
            }
        }
    }
}
