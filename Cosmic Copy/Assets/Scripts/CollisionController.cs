﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class CollisionController : MonoBehaviour
{
    //Player reference
    private GameObject player;

    [Header("UI elements")]
    [Tooltip("Game over display for how many clones were spawned. An analog for how long it took to win/lose")]
    public GameObject cloneCountText;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject loseTextObject;


    private int count;

    //Can the player restart?
    private bool canRestart = false;
    

    void Start()
    {
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
        cloneCountText.SetActive(false);

        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if(Input.GetKey("escape")){
            Application.Quit();
        }

        //if we won...
        if (canRestart)
        {
            //Only then allow pressing R to restart the game
            if (Input.GetKey("r"))
            {
                SceneManager.LoadScene(0);
            }
            //Wouldn't want an accidental bump of R to ruin your run.
        }

    }

    void SetCountText()
    {
        countText.text = "Coins: " + count + "/10";
        if(count >= 10)
        {
            winTextObject.SetActive(true);
            //Despawn all clones
            //Not really needed, I think it's fun to see them keep running around
            canRestart = true;

            //ooo lets display the total number of clones spawned, as a challenge. Can you do it in less clones?
            GameObject[] allClones = GameObject.FindGameObjectsWithTag("Clones");
            int totalClonesSpawned = allClones.Length;
            cloneCountText.GetComponent<TextMeshProUGUI>().text = totalClonesSpawned + " clones spawned";
            cloneCountText.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Coin"))
        {
           other.gameObject.SetActive(false); 
           count = count + 1;
           SetCountText();
        }
        else if(other.gameObject.CompareTag("Clones"))
        {
            //Only do this once
            if (!canRestart)
            {
                loseTextObject.SetActive(true);
                GameLost();
            }
        }
    }

    //Game over, allow the player to restart the game, but disable their WASD and Jumping, so the clones all merge into them.
    private void GameLost()
    {
        //Yes to restart
        canRestart = true;

        //Disable player movement inputs
        player.GetComponent<Invector.vCharacterController.vThirdPersonController>().gameIsLost = true;

        //Display the total number of clones spawned, as a challenge. Can you do it in less clones?
        GameObject[] allClones = GameObject.FindGameObjectsWithTag("Clones");
        int totalClonesSpawned = allClones.Length;
        cloneCountText.GetComponent<TextMeshProUGUI>().text = totalClonesSpawned + " clones spawned";
        cloneCountText.SetActive(true);
    }
}
