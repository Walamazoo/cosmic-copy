﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneMovement : MonoBehaviour
{
    //This is a GameObject because the parent could be the player, which wont have the CloneMovement Script on it.
    //Also we probably don't need a parent reference, but just in case...
    [Tooltip("The clone this clone gets its movement list from.")]
    public GameObject parent;

    [Tooltip("The clone this clone sends its movement list to")]
    public CloneMovement child;

    [Tooltip("The queue we will modify")]
    private Queue<Vector3> currentPositionsList;
    //same thing but for rotations
    private Queue<Quaternion> currentRotationsList;

    [Tooltip("The queue we will preserve in order to send to the next clone")]
    private Queue<Vector3> staticPositionsList;
    //Same thing again but for rotations
    private Queue<Quaternion> staticRotationsList;

    [Tooltip("This needs to be the same as the recordInterval on the PositionRecorder or stuffs going to break")]
    public float timeBetweenPositions = 0.2f;


    private float moveTimer;

    //Called when the game is lost, to freeze all clones in place.
    private bool canMove = true;

    private void Awake()
    {
        moveTimer = timeBetweenPositions;
    }

    //This needs to be outside of Update so we can call it in the if.
    private Vector3 nextPosition;
    private Quaternion nextRotation;

    // Update is called once per frame
    void Update()
    {
        //If we can move, move. lol
        if (canMove)
        {
            Move();
        }

    }

    //Move the clones!
    private void Move()
    {
        if (moveTimer > 0)
        {
            moveTimer -= Time.deltaTime;

            //Lerping needs to happen here.
            if (nextPosition == Vector3.zero)
            {
                //If it's Vector3.zero, then this is the clone's first movement, and it doesn't have a real position to move to yet.
                //Debug.Log("I'm trying to lerp through world center!");
            }
            else
            {
                //Lerp time!
                this.transform.position = Vector3.Lerp(this.transform.position, nextPosition, (Time.deltaTime / moveTimer));
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, nextRotation, (Time.deltaTime / moveTimer));
            }
        }
        else
        {
            //Pop the next position
            try
            {
                nextPosition = currentPositionsList.Dequeue();
                nextRotation = currentRotationsList.Dequeue();
                moveTimer = timeBetweenPositions;
            }
            catch (System.InvalidOperationException)
            {
                //The queue is empty because reasons?
            }
            catch (System.NullReferenceException)
            {
                //Why would this be null after a few clones? hmm....
                //Debug.Log(this.name + " at position " + this.transform.position + " is reporting a " +currentPositionsList + " list");

                //The answer was the slight delay in the sending of the first list. which I have now reduced, so this catch is not getting called.
                //But I'm leaving it here anyways just to make  anote of it.
            }

        }
    }


    public void SendList(Queue<Vector3> newPositionList, Queue<Quaternion> newRotationList)
    {
        if (child == null)
        {
            //Then this is the last clone, so don't try to send the list
        }
        else
        {
            //Send the child the old list
            child.SendList(staticPositionsList, staticRotationsList);
        }
        //Set our current list to the new one.
        currentPositionsList = new Queue<Vector3>(newPositionList);
        staticPositionsList = newPositionList;
        //Set our current list to the new one.
        currentRotationsList = new Queue<Quaternion>(newRotationList);
        staticRotationsList = newRotationList;


        //Just a debug print statement
        /*
        string debugText = "";
        foreach(Vector3 spot in currentPositionsList)
        {
            debugText += " " + spot;
        }

        Debug.Log(this.name + " recieved a new list of positons " + debugText);
        */
    }



    public void FreezeClones()
    {
        canMove = false;
    }
}
