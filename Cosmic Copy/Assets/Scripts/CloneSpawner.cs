﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("The prefab for spawning")]
    public GameObject clone;

    public GameObject player;
    [Tooltip("Where to spawn the clones")]
    public Transform spawnLocation;


    [Tooltip("How many clones to spawn. Make -1 for Infinite clones.")]
    public int clonesToSpawn = -1;

    //How many clones have been spawned.
    private int spawnedClones = 0;

    // Update is called once per frame
    void Update()
    {
        
    }

    private GameObject oldClone;

    private void SpawnClone()
    {
        //If we're not spawning an infinite amount, and we hit the limit...
        if(clonesToSpawn != -1 && spawnedClones >= clonesToSpawn)
        {
            //Stop spawning clones
            CancelInvoke();
            return;
        }

        //Create the clone
        GameObject newClone = GameObject.Instantiate(clone, spawnLocation.position, spawnLocation.rotation);
        spawnedClones++;

        //If this is the first clone, it's parent needs to be the player
        if(oldClone == null)
        {
            newClone.GetComponent<CloneMovement>().parent = player;
            oldClone = newClone;
            //Tell the player where to send the list
            player.GetComponent<PositionRecorder>().firstClone = oldClone.GetComponent<CloneMovement>();
        }
        else
        {
            //Set the references
            newClone.GetComponent<CloneMovement>().parent = oldClone;
            oldClone.GetComponent<CloneMovement>().child = newClone.GetComponent<CloneMovement>();

            oldClone = newClone;
        }

        //Debug.Log("Created Clone at time " + Time.time);

    }

    /// <summary>
    /// Called by the Position Recorder, so they start at the same time
    /// </summary>
    public void StartSpawning(float spawnInterval)
    {
        InvokeRepeating("SpawnClone", spawnInterval, spawnInterval);
    }
}
